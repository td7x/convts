const fs = require("fs");
const home = require("@td7x/home-court").path;
const path = require("path");
const shell = require("shelljs");
const url = require("url");

let args = ["install", "--save-dev", "--silent"];
let installer = "npm";
const pkg = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, "package.json"), "utf8")
);

let repo =
  typeof pkg.repository === "string"
    ? pkg.repository
    : url.parse(pkg.repository.url || " ").hostname;

let packages = Object.keys(pkg.peerDependencies).map(function(key) {
  return `${key}@${pkg.peerDependencies[key]}`;
});

const hasYarn = fs.existsSync(path.resolve(home, "yarn.lock"));
if (hasYarn) {
  installer = "yarn";
  args = ["add", "--dev", "--silent"];
}

args = args.concat(packages);

console.log(`\n
${pkg.name} installing ${home} with:\n${installer} ${args.join(" ")}
`);
console.log("this might take a bit ...");

shell.cd(home);
shell.exec(`${installer} ${args.join(" ")}`);

console.log(`
Put the TypeScript tools to work by configuring them.
Look to https://gitlab.com/td7x/convts for an example.
`);
