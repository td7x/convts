# @td7x/convts
Tom's Conventional TypeScript

## Installation

Install runs a script that installs this package's peerDeps as the apps own 
devDependencies (flat). Manual config is currently needed.

``` bash
$ npm i -D @td7x/convts
```

> On the first run you will still receive the unmet peer peerDependencies 
warning because the install script is not run as preinstall due to its own deps.

## Usage

.lintstagedrc
```json
{
  "src/**/*": [
    "tslint --fix",
    "prettier --write",
    "git add"
  ],
  ".gitlab-ci.yml": [
    "gitlab-ci-lint"
  ]
}
```

.prettierrc
```json
{
  "trailingComma": "es5",
  "listDifferent": true
}
```

tslint.json
```json
{
  "extends": ["@td7x/tslint-config"]
}
```

tsconfig.json
```json
{
  "compileOnSave": true,
  "compilerOptions": {
    "outDir": "./build/main",
    "rootDir": "./src",
    "module": "commonjs",
    "declaration": true,
    "moduleResolution": "node",
    "importHelpers": true,
    "inlineSourceMap": true,
    "listFiles": true,
    "traceResolution": true,
    "pretty": true,
    "lib": [
      "es6"
    ],
    "types": [
      "node"
    ],
    "baseUrl": ".",
    "paths": {
      "typescript-starter": ["src/index.ts"]
    }
  },
  "include": [
    "src/**/*.ts"
  ],
  "exclude": [
    "src/**/*spec.ts"
  ]
}
```

typedoc.json
```json
{
  "readme": "README.md",
  "emitDecoratorMetadata": true,
  "excludeExternals": true,
  "experimentalDecorators": true,
  "hideGenerator": true,
  "ignoreCompilerErrors": true,
  "includeDeclarations": false,
  "mode": "file",
  "exclude": "src/**/*spec.ts",
  "theme": "minimal",
  "module": "commonjs",
  "moduleResolution": "node",
  "out": "build/docs",
  "preserveConstEnums": true,
  "stripInternal": true,
  "suppressExcessPropertyErrors": true,
  "suppressImplicitAnyIndexErrors": true,
  "target": "ES6"
}
```

package.json
```json
  "scripts": {
    "build": "npm run build:cjs && npm run build:js && npm run build:docs",
    "build:cjs": "trash build/main && tsc",
    "build:js": "trash build/module && tsc -p other/tsconfig.module.json",
    "build:docs": "trash build/docs && typedoc typedoc --options other/typedoc.html.json src",
    "lint": "tslint --fix src/**/*.ts",
    "test": "npm run lint && npm run test:e2e",
    "test:unit": "NODE_ENV=test nyc mocha src/**/*.spec.ts",
    "test:e2e": "NODE_ENV=test nyc mocha src/**/*spec.ts",
    "dev": "npm run test:unit -- --watch",
    "reset": "git clean -dfx && git reset --hard && npm i",
    "docs": "npm run build:docs && opn ./docs/index.html",
    "version": "standard-version",
    "release": "git push --follow-tags origin master && conventional-gitlab-releaser -p angular",
    "commitmsg": "commitlint --extends @commitlint/config-angular -e",
    "precommit": "lint-staged",
    "prepush": "npm run test:unit"
  }
```
