# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/td7x/convts/compare/v0.2.0...v0.3.0) (2017-07-16)


### Features

* **ci:** config for lint and style ([4421804](https://gitlab.com/td7x/convts/commit/4421804))
* **core:** flat install peerDeps ([5b11e49](https://gitlab.com/td7x/convts/commit/5b11e49))
* **core:** update deps ([a88397d](https://gitlab.com/td7x/convts/commit/a88397d))
* **core:** update deps & move to [@td7x](https://github.com/td7x)/tslint-config ([769f0ab](https://gitlab.com/td7x/convts/commit/769f0ab))
* **docs:** typedoc expample ([adb1974](https://gitlab.com/td7x/convts/commit/adb1974))



<a name="0.2.0"></a>
# 0.2.0 (2017-06-19)


### Features

* **core:** initial commit ([26c21eb](https://gitlab.com/td7x/convts/commit/26c21eb))



<a name="0.3.1"></a>
## [0.3.1](https://gitlab.com/td7x/convr/compare/v0.3.0...v0.3.1) (2017-06-19)



<a name="0.3.0"></a>
# [0.3.0](https://gitlab.com/tom.davidson/convr/compare/v0.1.1...v0.3.0) (2017-06-19)


### Features

* **core:** new gitlab-lint-ci ([9ab81ef](https://gitlab.com/tom.davidson/convr/commit/9ab81ef))



<a name="0.1.1"></a>
## [0.1.1](https://gitlab.com/tom.davidson/convr/compare/v0.1.0-beta.1...v0.1.1) (2017-06-19)


### Bug Fixes

* **install:** disable automatic run script change on install ([380ebe1](https://gitlab.com/tom.davidson/convr/commit/380ebe1))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/tom.davidson/convr/compare/v0.1.0-beta.1...v0.1.0) (2017-06-19)


### Bug Fixes

* **install:** disable automatic run script change on install ([380ebe1](https://gitlab.com/tom.davidson/convr/commit/380ebe1))



<a name="0.1.0-beta.1"></a>
# [0.1.0-beta.1](https://gitlab.com/tom.davidson/convr/compare/v0.1.0...v0.1.0-beta.1) (2017-05-07)



<a name="0.1.0-alpha.0"></a>
# [0.1.0-alpha.0](https://gitlab.com/tom.davidson/convr/compare/0.1.0-alpha.0...v0.1.0-alpha.0) (2017-05-07)



<a name="0.1.0-beta.0"></a>
# [0.1.0-beta.0](https://gitlab.com/tom.davidson/convr/compare/v0.1.0...v0.1.0-beta.0) (2017-05-07)



<a name="0.1.0-alpha.0"></a>
# [0.1.0-alpha.0](https://gitlab.com/tom.davidson/convr/compare/v0.0.2...v0.1.0-alpha.0) (2017-05-07)



<a name="0.0.2"></a>
## 0.0.2 (2017-05-07)


### Bug Fixes

* **core:** normalized package.json ([c9a93c7](https://gitlab.com/tom.davidson/convr/commit/c9a93c7))
